use clap::Parser;

mod load_pokemon_list;

/// Search for a pattern in a file and display the lines that contain it.
#[derive(Parser)]
struct Cli {
    /// The pattern to look for
    pattern: String,
    /// The path to the file to read
    path: std::path::PathBuf,
}

#[tokio::main]
async fn main() {
    // let args = Cli::parse();
    // let content = std::fs::read_to_string(&args.path).expect("could not read file");

    let pokemon_results = load_pokemon_list::load_pokemon_list().await;

    for results in pokemon_results.results {
        println!("{}", results.name);
    }
}
