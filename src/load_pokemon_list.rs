#[derive(Debug, serde::Deserialize)]
pub struct PokemonData {
    pub name: String,
    pub url: String,
}

// Define a struct for the main data object
#[derive(Debug, serde::Deserialize)]
pub struct PokemonList {
    pub count: i32,
    pub next: Option<String>,
    pub previous: Option<String>,
    pub results: Vec<PokemonData>,
}

pub async fn load_pokemon_list() -> PokemonList {
    let client = reqwest::Client::new();
    let response = match client
        .get("https://pokeapi.co/api/v2/pokemon/")
        .send()
        .await
    {
        Ok(resp) => resp,
        Err(err) => panic!("Error: {}", err),
    };

    match response.status() {
        reqwest::StatusCode::OK => {
            println!("Success! {:?}", response.status());
            let response_body = response.json::<PokemonList>().await;
            // println!("Success! {:?}", response_body);
            match response_body {
                Ok(parsed) => {
                    // println!("Success! {:?}", parsed);
                    return parsed;
                }
                Err(e) => {
                    println!("Error: {}", e);
                    println!("Hm, the response didn't match the shape we expected.")
                }
            };
        }
        reqwest::StatusCode::UNAUTHORIZED => {
            println!("Need to grab a new token");
        }
        _ => {
            panic!("Uh oh! Something unexpected happened.");
        }
    };

    panic!("No list returned");
}
